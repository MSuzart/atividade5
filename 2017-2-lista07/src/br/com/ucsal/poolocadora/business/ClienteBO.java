package br.com.ucsal.poolocadora.business;

import java.util.List;

import br.com.ucsal.poolocadora.entity.Cliente;
import br.com.ucsal.poolocadora.persistence.ClienteDAO;

public class ClienteBO {
	public static void inserir(Cliente cliente) throws Exception {
		validarCampos(cliente);
		ClienteDAO.insert(cliente);
	}

	public static void delete(Cliente cliente) {
		ClienteDAO.delete(cliente);
	}

	public static void update(Cliente cliente) throws Exception {
		ClienteDAO.update(cliente);
	}

	public static List<Cliente> findAll() throws Exception {
		List<Cliente> clientes = ClienteDAO.findAll();
		if (clientes.isEmpty()) {
			throw new Exception("Ainda n�o possui clientes cadastrados");
		}
		return clientes;
	}

	private static void validarCampos(Cliente cliente) throws Exception {
		if (cliente.getNome() == null) {
			throw new Exception("Erro: Nome inexistente");
		}
		if (cliente.getCpf() == null) {
			throw new Exception("Erro: CPF inexistente");
		}
		if (cliente.getCpf().length() != 11) {
			throw new Exception("Erro: CPF inv�lido");
		}
		verificarCpfRepetido(cliente);
		if (cliente.getEndereco() == null) {
			throw new Exception("Erro: Endere�o inexistente");
		}
	}

	private static void verificarCpfRepetido(Cliente cliente) throws Exception {
		List<Cliente> clientes = ClienteDAO.findAll();
		for (Cliente clienteLista : clientes) {
			if (clienteLista.getCpf().equals(cliente.getCpf())) {
				throw new Exception("CPF j� existe");
			}
		}

	}

	public static Cliente findByCPF(String cpf) throws Exception {
		Cliente cliente = ClienteDAO.findByCPF(cpf);
		return cliente;
	}
}
