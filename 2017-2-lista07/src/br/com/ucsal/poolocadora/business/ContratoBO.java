package br.com.ucsal.poolocadora.business;

import java.util.Date;
import java.util.List;

import br.com.ucsal.poolocadora.entity.Contrato;
import br.com.ucsal.poolocadora.entity.ValorDiarioTipoVeiculo;
import br.com.ucsal.poolocadora.entity.Veiculo;
import br.com.ucsal.poolocadora.enums.ETipo;
import br.com.ucsal.poolocadora.persistence.ContratoDAO;
import br.com.ucsal.poolocadora.persistence.ValorDiarioTipoVeiculoDAO;
import br.com.ucsal.poolocadora.persistence.VeiculoDAO;

public class ContratoBO {

	public static void inserir(Contrato contrato) throws Exception {
		validarCampos(contrato);
		somarValorTotal(contrato);
		ContratoDAO.insert(contrato);
	}

	public static void delete(Contrato contrato) {
		ContratoDAO.delete(contrato);
	}

	public static void update(Contrato contrato) {
		ContratoDAO.update(contrato);
	}

	public List<Contrato> findByClientesOrdenado() {
		return ContratoDAO.findByClientesOrdenado();
	}

	public Contrato findByNumeroContrato(Integer numeroContrato) throws Exception {
		return ContratoDAO.findByNumeroContrato(numeroContrato);
	}

	public static List<Contrato> findByCPF(String cpf) throws Exception {
		if (cpf == null) {
			throw new Exception("O CPF n�o pode ser null");
		}
		return ContratoDAO.findByCPF(cpf);
	}

	private static void validarCampos(Contrato contrato) throws Exception {
		if (contrato.getCliente() == null) {
			throw new Exception("Cliente inexistente");
		}
		ClienteBO.findByCPF(contrato.getCliente().getCpf());
		if (contrato.getNumContrato() == null) {
			throw new Exception("Contrato Inv�lido");
		}
		if (contrato.getQtdDiasLocacao() == null) {
			throw new Exception("A quantidade de dias de loca��o n�o pode ser nulo");
		}
		if (contrato.getDataContrato() == null) {
			throw new Exception("A data do contrato n�o pode ser nulo");
		}
		if (contrato.getVeiculos() == null) {
			throw new Exception("Ve�culo inexistente");
		}
		for (Veiculo veiculo : contrato.getVeiculos()) {
			VeiculoDAO.findByVeiculo(veiculo);
		}
	}

	private static Double somarValorTotal(Contrato contrato) {
		Double valorTotal = 0D;
		for (Veiculo veiculo : contrato.getVeiculos()) {
			valorTotal += valorTipoVeiculo(contrato.getDataContrato(), veiculo.getTipo());
		}
		return valorTotal;
	}

	private static Double valorTipoVeiculo(Date date, ETipo tipo) {
		for (ValorDiarioTipoVeiculo valorDiarioTipoVeiculo : ValorDiarioTipoVeiculoDAO.findAll()) {
			if (date.equals(valorDiarioTipoVeiculo.getData()) && tipo.equals(valorDiarioTipoVeiculo.getTipoVeiculo())) {
				return valorDiarioTipoVeiculo.getValor();
			}
		}
		return ETipo.valorTipoVeiculo(tipo);

	}
}
