package br.com.ucsal.poolocadora.business;

import java.util.List;

import br.com.ucsal.poolocadora.entity.Veiculo;
import br.com.ucsal.poolocadora.persistence.VeiculoDAO;

public class VeiculoBO {

	public static void inserir(Veiculo veiculo) throws Exception {
		validarCampos(veiculo);
		VeiculoDAO.insert(veiculo);
	}

	public static void delete(Veiculo veiculo) throws Exception {
		findByVeiculo(veiculo);
		VeiculoDAO.delete(veiculo);
	}

	public static void update(Veiculo veiculo) throws Exception {
		findByVeiculo(veiculo);
		VeiculoDAO.update(veiculo);
	}

	private static void validarCampos(Veiculo veiculo) throws Exception {
		if (veiculo.getAnoFabricacao() == null) {
			throw new Exception("O ano de fabrica��o n�o pode ser nulo");
		}
		if (veiculo.getPlaca() == null) {
			throw new Exception("A placa n�o pode ser nulo");
		}
		validarPlacaRepetida(veiculo);
		if (veiculo.getTipo() == null) {
			throw new Exception("O tipo n�o pode ser nulo");
		}
	}

	private static void validarPlacaRepetida(Veiculo veiculo) throws Exception {
		List<Veiculo> veiculos = VeiculoDAO.findAll();
		for (Veiculo veiculoLista : veiculos) {
			if (veiculo.getPlaca().equals(veiculoLista.getPlaca())) {
				throw new Exception("Ve�culo com placa j� existente");
			}
		}
	}

	public static Veiculo findByVeiculo(Veiculo veiculo) throws Exception {
		if (VeiculoDAO.findByVeiculo(veiculo) == null) {
			throw new Exception("Ve�culo n�o cadastrado");
		}
		return veiculo;
	}

}
