package br.com.ucsal.poolocadora.enums;

public enum ETipo {
	BASICO("Bsico", 1, 100.45D), INTERMEDIARIO("Intermediário", 2, 130.10D), LUXO("Luxo", 0, 156.0D);

	private String descricao;
	private int index;
	private Double valor;

	private ETipo(String descricao, int index, Double valor) {
		this.descricao = descricao;
		this.index = index;
		this.setValor(valor);
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public static Double valorTipoVeiculo(ETipo tipo) {
		if (tipo.equals(BASICO)) {
			return BASICO.valor;
		}
		if (tipo.equals(INTERMEDIARIO)) {
			return INTERMEDIARIO.valor;
		}

		return LUXO.valor;

	}

}
