package br.com.ucsal.poolocadora.entity;

import br.com.ucsal.poolocadora.enums.ETipo;

public class Veiculo {
	private String placa;
	private Integer anoFabricacao;
	private ETipo tipo;

	@Override
	public boolean equals(Object obj) {
		Veiculo veiculo = (Veiculo)obj;
		if(veiculo.placa==this.placa) {
			return true;
		}
		return false;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public ETipo getTipo() {
		return tipo;
	}

	public void setTipo(ETipo tipo) {
		this.tipo = tipo;
	}

}
