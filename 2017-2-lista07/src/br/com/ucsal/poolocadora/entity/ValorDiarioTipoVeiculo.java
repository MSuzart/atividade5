package br.com.ucsal.poolocadora.entity;

import java.util.Date;

import br.com.ucsal.poolocadora.enums.ETipo;

public class ValorDiarioTipoVeiculo {
	private Date data;
	private ETipo tipoVeiculo;
	private Double valor;

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public ETipo getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(ETipo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

}
