package br.com.ucsal.poolocadora.persistence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.ucsal.poolocadora.entity.Contrato;

public class ContratoDAO {
	private static List<Contrato> contratos = new ArrayList<Contrato>();

	public static List<Contrato> findAll() {
		return contratos;
	}

	public static void insert(Contrato contrato) {
		contratos.add(contrato);
	}

	public static void delete(Contrato contrato) {
		contratos.remove(contrato);
	}

	public static void update(Contrato contrato) {
		// FIXME identificar o contrato para depois edita-lo
		contratos.add(contrato);
	}

	public static List<Contrato> findByCPF(String cpf) throws Exception {
		List<Contrato> contratosCPF = new ArrayList<Contrato>();
		for (Contrato contrato : contratos) {
			if (contrato.getCliente().getCpf().equals(cpf)) {
				contratosCPF.add(contrato);
			}
		}
		if (!contratosCPF.isEmpty()) {
			return contratosCPF;
		}
		throw new Exception("Contrato com CPF " + cpf + " n�o encontrado");
	}

	public static Contrato findByNumeroContrato(Integer numeroContrato) throws Exception {
		for (Contrato contrato : contratos) {
			if (contrato.getNumContrato().equals(numeroContrato)) {
				return contrato;
			}
		}
		throw new Exception("N�o foi poss�vel encontrar o contrato com o n�mero " + numeroContrato);
	}

	public static List<Contrato> findByClientesOrdenado() {
		Collections.sort(contratos);
		return contratos;
	}

}
