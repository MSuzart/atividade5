package br.com.ucsal.poolocadora.persistence;

import java.util.ArrayList;
import java.util.List;

import br.com.ucsal.poolocadora.entity.Veiculo;

public class VeiculoDAO {
	private static List<Veiculo> veiculos = new ArrayList<Veiculo>();

	public static List<Veiculo> findAll() {
		return veiculos;
	}

	public static void insert(Veiculo veiculo) {
		veiculos.add(veiculo);
	}

	public static void delete(Veiculo veiculo) {
		veiculos.remove(veiculo);
	}


	public static void update(Veiculo veiculo) {
		// FIXME identificar o veiculo para depois edita-lo
		veiculos.add(veiculo);
	}

	public static Veiculo findByVeiculo(Veiculo veiculo) {
		if (veiculos.contains(veiculo)) {
			return veiculo;
		}
		return null;
	}
}
