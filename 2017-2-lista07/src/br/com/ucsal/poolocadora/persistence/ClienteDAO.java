package br.com.ucsal.poolocadora.persistence;

import java.util.ArrayList;
import java.util.List;

import br.com.ucsal.poolocadora.entity.Cliente;

public class ClienteDAO {
	private static List<Cliente> clientes = new ArrayList<Cliente>();

	public static List<Cliente> findAll() {
		return clientes;
	}

	public static void insert(Cliente cliente) throws Exception {
		clientes.add(cliente);
	}

	public static void delete(Cliente cliente) {
		clientes.remove(cliente);
	}

	public static void update(Cliente cliente) throws Exception {
		// FIXME identificar o cliente para depois edita-lo
		clientes.add(cliente);
	}

	public static Cliente findByCPF(String cpf) throws Exception {
		for (Cliente cliente : clientes) {
			if (cliente.getCpf().equals(cpf)) {
				return cliente;
			}
		}
		throw new Exception("Cliente inexistente");
	}
	

}
