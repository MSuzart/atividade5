package br.com.ucsal.poolocadora.persistence;

import java.util.ArrayList;
import java.util.List;

import br.com.ucsal.poolocadora.entity.ValorDiarioTipoVeiculo;

public class ValorDiarioTipoVeiculoDAO {
	private static List<ValorDiarioTipoVeiculo> valorDiarioTipoVeiculos = new ArrayList<ValorDiarioTipoVeiculo>();

	public static List<ValorDiarioTipoVeiculo> findAll() {
		return valorDiarioTipoVeiculos;
	}
	
}
