package br.com.ucsal.poolocadora.interfaces;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class ViewClienteTeste extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8784599213112085646L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewClienteTeste frame = new ViewClienteTeste();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ViewClienteTeste() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(UIManager.getBorder("Button.border"));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setForeground(Color.LIGHT_GRAY);
		panel.setBounds(0, 0, 434, 61);
		contentPane.add(panel);
		
		JLabel lblCadastrarContrato = new JLabel("Cadastrar Cliente");
		lblCadastrarContrato.setFont(new Font("Arial Black", Font.PLAIN, 28));
		panel.add(lblCadastrarContrato);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setBounds(20, 131, 46, 14);
		contentPane.add(lblCpf);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(20, 106, 46, 14);
		contentPane.add(lblNome);
		
		JLabel lblEndereo = new JLabel("Endere\u00E7o");
		lblEndereo.setBounds(20, 156, 66, 14);
		contentPane.add(lblEndereo);
		
		textField = new JTextField();
		textField.setBounds(96, 103, 302, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(96, 128, 302, 20);
		contentPane.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(96, 153, 302, 20);
		contentPane.add(textField_2);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setBounds(269, 227, 129, 23);
		contentPane.add(btnCadastrar);
	}
}
