package br.com.ucsal.poolocadora.interfaces;

import java.util.List;

import br.com.ucsal.poolocadora.business.ClienteBO;
import br.com.ucsal.poolocadora.entity.Cliente;

public class ClienteTUI {
	public static void main(String[] args) {
		// Cadastrar Cliente
		cadastrarCliente();

		// Buscar Cliente
		buscarCliente();

		// Buscar Por Cpf
		buscarClienteCPF("32146848954");
		
		// Remover Cliente
		removerCliente("32146848956");
	}

	private static void removerCliente(String cpf) {
		try {
			Cliente cliente = ClienteBO.findByCPF(cpf);
			ClienteBO.delete(cliente);
			System.out.println("Cliente Removido com Sucesso");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}

	private static void buscarClienteCPF(String cpf) {
		try {
			Cliente cliente = ClienteBO.findByCPF(cpf);
			System.out.println("CPF: " + cliente.getCpf());
			System.out.println("Nome: " + cliente.getNome());
			System.out.println("Endere�o " + cliente.getEndereco());
			System.out.println("---------------------------------------------");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private static void buscarCliente() {
		try {
			List<Cliente> clientes = ClienteBO.findAll();
			for (Cliente cliente : clientes) {
				System.out.println("CPF: " + cliente.getCpf());
				System.out.println("Nome: " + cliente.getNome());
				System.out.println("Endere�o " + cliente.getEndereco());
				System.out.println("---------------------------------------------");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private static void cadastrarCliente() {
		Cliente cliente = new Cliente();
		cliente.setCpf("32146848956");
		cliente.setEndereco("Caixa D'Agua");
		cliente.setNome("Carlos Pedro Michel");
		try {
			ClienteBO.inserir(cliente);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
