package teste;

import br.com.ucsal.poolocadora.entity.Cliente;

public class ClienteBuilder {

    public static final String CPF = "12345678900";
    public static final String NOME = "Joao";
    public static final String ENDERECO = "Rua A";

    private String endereco = ENDERECO;
    private String nome = NOME;
    private String cpf = CPF;

    private ClienteBuilder() {
    }

    public static ClienteBuilder aUser() {
        return new ClienteBuilder();
    }

    public ClienteBuilder withName(String nome) {
        this.nome = nome;
        return this;
    }

    public ClienteBuilder withEndereco(String endereco) {
        this.endereco = endereco;
        return this;
    }

    public ClienteBuilder withCpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public ClienteBuilder but() {
        return ClienteBuilder
                .aUser()
                .withName(nome)
                .withEndereco(endereco)
                .withCpf(cpf);
    }

    public Cliente build() {
        return new Cliente(cpf, nome, endereco);
    }
}
